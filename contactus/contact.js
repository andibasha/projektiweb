const form = document.getElementById('form');
const yourname = document.getElementById('yourname');
const yourphone = document.getElementById('yourphone');
const youremail = document.getElementById('youremail');
const subject = document.getElementById('subject');



form.addEventListener('submit', e => {
	e.preventDefault();
	
	checkInputs();
});

function checkInputs() {
	// trim to remove the whitespaces
	const yournameValue = yourname.value.trim();
    const yourphoneValue = yourphone.value.trim();
    const youremailValue = youremail.value.trim();
	const subjectValue = subject.value.trim();
	
	if(yournameValue === '') {
		setErrorFor(username, 'Your Name cannot be blank');
	} else {
		setSuccessFor(username);
    }
    
	
	if(yourphoneValue === '') {
		setErrorFor(password, 'Phone Number cannot be blank');
	} else {
		setSuccessFor(password);
    }

    if(youremailValue === '') {
		setErrorFor(email, 'Email cannot be blank');
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Not a valid email');
	} else {
		setSuccessFor(email);
    }
    
    if(subjectValue === '') {
		setErrorFor(password, 'Subject cannot be blank');
	} else {
		setSuccessFor(password);
    }
}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}

function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}













// SOCIAL PANEL JS
const floating_btn = document.querySelector('.floating-btn');
const close_btn = document.querySelector('.close-btn');
const social_panel_container = document.querySelector('.social-panel-container');

floating_btn.addEventListener('click', () => {
	social_panel_container.classList.toggle('visible')
});

close_btn.addEventListener('click', () => {
	social_panel_container.classList.remove('visible')
});