const topbar = document.querySelector(".topNav");
const element = document.querySelector("#first");
const element1 = document.querySelector("#second");
const element2 = document.querySelector("#third");
const element3 = document.querySelector("#fourth");
const element4 = document.querySelector("#fifth");
let x=0;
let y=0;
let z=0;
let v=0;
let h=0;

var styles = {
    "background-color" : "white",
    "transition ": "all ease-in .2s;"
}

$('.post-wrapper').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 900,
    nextArrow: $('.next'),
    prevArrow: $('.prev'),
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
  });

//topbar.style.backgroundColor = "lightgray";